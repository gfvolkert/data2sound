# README #

### Motivation ###

* Every day petabytes of data is produced worldwide and summarized and visualized for reports. 
* What insights can be gleaned if this data is not looked - as usual - but listened to? 
* We use a programmatic approach to translate data into music automatically in terms of algorithmic compositions. 

### What is this repository for? ###

* Tools for Data Sonification based on Java.
* Transforms tsv and csv data sets into midi files
* The present code has been written for the data sonification project 
* https://soundcloud.com/user-539555843
* With slight modifications it could be reused for more generic data sets

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact