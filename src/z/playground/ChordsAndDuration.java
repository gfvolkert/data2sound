package z.playground;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import jm.music.data.CPhrase;
import jm.music.data.Part;
import jm.music.data.Score;
import jm.util.Write;
import midi.abstraction.NoteExtended;
import midi.abstraction.StacksOfNotesFactory;

public class ChordsAndDuration {

	public static void main(String[] args) {
		
		Score score = new Score();
		int nrOfChords = 4;
		int nrOfIntruments = 2;
		CPhrase[] cphrArray = new CPhrase[nrOfIntruments];
		for (int i = 0; i < cphrArray.length; i++) {
			cphrArray[i] = new CPhrase();
		}
		
		double dur=StacksOfNotesFactory.durationFactor;
		
		for (int day = 0; day < nrOfChords; day++) {
			//int dur= 1;
		
			for(CPhrase cphr : cphrArray){
	
				NoteExtended[] noteChord = new NoteExtended[3];
				
				/**
				 * IMPORTANT: Order plays a crucial role when taking into account "global" duration.
				 * Set always the Note with longest duration at the first entry! 
				 */
				noteChord[0] = new NoteExtended(60, dur*7, 127);
				noteChord[0].setRhythmValue(0.5);
				noteChord[1] = new NoteExtended(64, dur*3, 60);
				noteChord[0].setRhythmValue(0.5);
				noteChord[2] = new NoteExtended(67, dur, 10);				
				noteChord[0].setRhythmValue(0.5);		
				cphr.addChord(noteChord);
					
//					int[] chord = new int[3];
//					int k=0;
//	
//					int maxDynamic = 127;
//					
//					maxDuration=maxDuration*dur;
//					dur=dur*2;
//					
//					chord[0]=60;
//					chord[1]=chord[0]+4;
//					chord[2]=chord[0]+7;
//								
//					cphr.addChord(chord,maxDuration, maxDynamic);
					
			}
		}
			
				Part[] partArray = new Part[nrOfIntruments];
				
				for (int i = 0; i < nrOfIntruments; i++) {
					int instrNr= 41+i;
					//partArray[i] = new Part("Instrument "+instrNr,instrNr,phrArray[i]);
					partArray[i] =new Part("Instrument "+instrNr,instrNr);
					partArray[i].addCPhrase(cphrArray[i]);
					partArray[i].setChannel(i);
				}
			
				
				score.addPartList(partArray);
				
//				Play.midi(score);
				Write.midi(score, "ChordTest.mid");
				try {
					Desktop.getDesktop().open(new File("ChordTest.mid"));
				} catch (IOException e) {
					e.printStackTrace();
				}
		
		}
	

}
