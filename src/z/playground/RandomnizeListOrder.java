package z.playground;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RandomnizeListOrder {

	public static void useHashSet(List<Integer> list){
		Set<Integer> set = new HashSet<Integer>(list);
		System.err.println(set);
		List<Integer> list2 = new ArrayList<Integer>(set); 
		list.clear();
		list.addAll(list2);
	}
	
	public static void useCollectionShufffle(List<Integer> list){
		Collections.shuffle(list);
	}
	
	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>(); 
		for (int i = 0; i < 5; i++) {
			list.add(i);
		}
		System.out.println(list);
		useCollectionShufffle(list);
		//useHashSet(list);
		System.out.println(list);
	}

}
