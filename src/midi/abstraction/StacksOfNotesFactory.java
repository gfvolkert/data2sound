package midi.abstraction;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Once the mapping 'categories2ListOfNotes' is generated via the constructor,
 * one may generate Sets of randomized Notes. From the latter one may generate
 * Stacks of randomized Notes.
 */
public class StacksOfNotesFactory {

	public Map<Integer, List<NoteExtended>> categories2ListOfNotes;

	/**
	 * nrOfIntruments is determined by the inequality:
	 * maximalNumberOfmaxNumbersOfEvents4EachCategory =< Anz.Oktaven x
	 * Anz.TöneSkala x Anz. Intrumente
	 */
	public int nrOfIntruments;

	private final int[] _scale = { 0, 2, 3, 4 };// myScale//{0,1,2,3,4,5,6,7,8,9,10,11};//
	// private final int[] _scale = {0, 2, 3, 5, 7, 8, 10};//
	// jm.JMC.MINOR_SCALE;
	// private final int[] _scale ={0, 2, 4, 7, 9};// jm.JMC.PENTATONIC_SCALE;
	
	//private final int maxNrOfOctavesForEachCategory = 2;

	private int maxClaviaturValue =103;
	private int minClaviaturValue =12;

	

	// log(nk)*0.125

	public static double globalRythmValue =  jm.constants.Durations.SIXTEENTH_NOTE;// corresponds duration
														// of one day
	public static final double durationFactor = jm.constants.Durations.THIRTYSECOND_NOTE;//minimal duration of Note
	public static int velocityFactor;
	//public static double maxDurationFactor;// may be longer than the duration of
											// one day
	public static double maxDurationValue=globalRythmValue*8;
	
	public static int numberOfCategories;

	

	/**
	 * Constructor
	 * 
	 * @param Integer[]
	 *            maximal number Of events for each Category
	 */
	public StacksOfNotesFactory(Integer[] maxNrOfEvents4EachCategory) {

		//1.Compute available keyboard notes by extending a given scale on the keyboard with given min midi note to max midi note number in [0,127]: 
		List<Integer> scaleOnClaviatur = genScaleOnClaviatur(_scale, 0);

		//2.Compute the partition of the number of notes per category. 
		//2.1 The Sum of all maxNrOfEvents4EachCategory corresponds to 100%.
		//2.2 Divide for each category the maxNrOfEvents4EachCategory by the sum of 2.1
		//2.3 Multiply the resulting quotient of 2.2 with the size of the keyboard to the get the number of notes for the category
		Map<Integer, Integer> numbersOfNotesInEachCat = genKeyValuePairingCat2NrOfNotes(maxNrOfEvents4EachCategory,scaleOnClaviatur.size());
				
		//3.Compute the number of instruments necessary to distinguish all events of one day in a given categories 
		List<Integer> list = Arrays.asList(maxNrOfEvents4EachCategory);
		int maximalNrOfmaxNrOfEvents4EachCategory = Collections.max(list);

		//TODO: find index of maximumNrOfNotes: 
		int maxNrOfNotesForCategoryWithMaximalNrOfEvents = numbersOfNotesInEachCat.get(1);
		
		/**
		 * Formula for each category(!)
		 * maximalNumberOfmaxNumbersOfEvents4EachCategory =<
		 * maxNrOfNotesForCategoryWithMaximalNrOfEvents
		 */
		this.nrOfIntruments = maximalNrOfmaxNrOfEvents4EachCategory / maxNrOfNotesForCategoryWithMaximalNrOfEvents;
		System.out.println("NrOfInstruments:" + this.nrOfIntruments);

		Map<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();

		int indexOfScale = 0;

		numberOfCategories = maxNrOfEvents4EachCategory.length;
		for (int key = numberOfCategories; key > 0; key--) {
			List<Integer> value = new ArrayList<Integer>();
			for (int j = 0; j < numbersOfNotesInEachCat.get(key); j++) {
				if(indexOfScale>scaleOnClaviatur.size()){
					break;
				}
				//if (j < sizeOfScale * maxNrOfOctavesForEachCategory) {
					value.add(scaleOnClaviatur.get(indexOfScale));
					indexOfScale++;
				//}
				
			}
			map.put(key, value);
			System.out.println(key + "->" + numbersOfNotesInEachCat.get(key) + "->" + value);
		}
		
		categories2ListOfNotes = new HashMap<Integer, List<NoteExtended>>();
		for (int key : map.keySet()) {
			List<NoteExtended> value = new ArrayList<NoteExtended>();
			// //Higher Categories should sound with more instruments even
			// though they have less note values:
			// for (int i = 0; i < key; i++) {

//			for (int pitch : map.get(key)) {
			for (int instrumentIndex = 1; instrumentIndex < this.nrOfIntruments + 1; instrumentIndex++) {
				for (int pitch : map.get(key)) {

					// duration and velocity values are default values which will
					// be overwritten in the handling
					// of the event in dependence of the particular value of nk:
					double defaulDuration = 0;//durationFactor * key;
					int defaultVelocity =0;//velocityFactor * key;
					// pitch = pitchShifter(pitch);
					value.add(new NoteExtended(pitch, defaulDuration, defaultVelocity, globalRythmValue, instrumentIndex));
				}
				
			}
			// }
			categories2ListOfNotes.put(key, value);
		}
	}

	/**
	 * Produces StackOfNotes. These are random when taking the parameter random true.
	 * In addition one may decided reproduce the same randomness by setting the parameter withSeed true. 
	 * 
	 * @param random
	 * @param withSeed
	 * @return
	 */
	public Map<Integer, Deque<NoteExtended>> produceStacksOfNotes(boolean random, boolean withSeed) {

		
		
		// Randomnize, if random==true, the value of each key and copy to
		// Stacks:
		Map<Integer, Deque<NoteExtended>> stacksOfNotes = new HashMap<Integer, Deque<NoteExtended>>();
		for (int key : categories2ListOfNotes.keySet()) {
			Deque<NoteExtended> value = new ArrayDeque<NoteExtended>();
			List<NoteExtended> valueOfKey = new ArrayList<NoteExtended>(categories2ListOfNotes.get(key));
			if (random && !withSeed) {
				Collections.shuffle(valueOfKey);
			}
			if(random && withSeed){
				//Collections.shuffle(valueOfKey, randomNumbersFromASeed);
			    Collections.shuffle(valueOfKey, new Random(123));
			}
			value.addAll(valueOfKey);
			stacksOfNotes.put(key, value);
		}
		return stacksOfNotes;

	}

	/**
	 * Returns key-value pairing with key=categoryNr, value=maxNumber Of Notes for each category
	 * 
	 * @param maxNumbers
	 * @return
	 */
	private Map<Integer, Integer> genKeyValuePairingCat2NrOfNotes(Integer[] maxNumbers,int sizeOfClaviatur) {

		//Compute the 100%...(must be double value for division later on..)
		double sumOfAllmaxNumbers =0.0;
		for (int value : maxNumbers) {
			sumOfAllmaxNumbers =sumOfAllmaxNumbers+value;
		}
		
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		int catNr = 1;
		double percent;
		//...and find the corresponding partition of notes on the given claviatur.. 
		for (int maxNr : maxNumbers) {
			percent= maxNr/sumOfAllmaxNumbers;
			int value = (int) (sizeOfClaviatur*percent);
			if(value==0){
				value=1;
			}
			map.put(catNr, value);
			catNr++;
		}
		return map;
	}

	private List<Integer> genScaleOnClaviatur(int[] _scale, int distortionFactor) {

		if (maxClaviaturValue  > 127) {
			System.err.println("maxClaviaturValue cannot exceed the value of 127");
			maxClaviaturValue = 127;
		}

		List<Integer> scale = new ArrayList<Integer>();
		int shift = -1;
		int i = 0;
		int size_of_scale = _scale.length;
		int pitch;
		
		while (_scale[i % size_of_scale] + shift * 12 + distortionFactor < minClaviaturValue/2) {
			shift++;
		}

		while (_scale[i % size_of_scale] + shift * 12 + distortionFactor < maxClaviaturValue) {
			if (i % size_of_scale == 0) {
				shift++;
			}
			pitch = _scale[i % size_of_scale] + shift * 12 + distortionFactor;
			if (pitch < maxClaviaturValue) {
				scale.add(pitch);
			}
			i++;
		}
		System.out.println("Scale:" + scale);
		return scale;

	}

	public static void main(String a[]) {

		Integer[] maxNr2Cat = { 80, 26, 16, 8, 6, 2, 2 };
		StacksOfNotesFactory factory = new StacksOfNotesFactory(maxNr2Cat);
		Map<Integer, Deque<NoteExtended>> stacksOfNotes = factory.produceStacksOfNotes(true, true);
		System.out.println("---");
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());
		stacksOfNotes = factory.produceStacksOfNotes(true, true);
		System.out.println("---");
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());
		stacksOfNotes = factory.produceStacksOfNotes(true, true);
		System.out.println("---");
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());
		stacksOfNotes = factory.produceStacksOfNotes(true, true);
		System.out.println("---");
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());
		System.out.println(stacksOfNotes.get(1).pop());

	}

}
