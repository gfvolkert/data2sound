package midi.abstraction;


import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.model.Category;
import data.model.GTDB;
import data.model.GTEvent;
import data.model.NK2MaxAtOneDay;
import data.simulation.GTDBMock;
import data.simulation.GTDBTestData;
import data.univocity.parsers.CSVTextParser;
import jm.audio.Instrument;
import jm.music.data.CPhrase;
import jm.music.data.Note;
import jm.music.data.Part;
import jm.music.data.Phrase;
import jm.music.data.Rest;
import jm.music.data.Score;
import jm.util.Play;
import jm.util.Write;

public class Data2Midi {
	
	public static boolean turnOnMidiFileWriter = true;//false;  

	public List<Category> listOfCategories;
	//public Map<Integer, List<Integer>> nk2NotesMapping;
	public Map<Integer, List<Integer>> nk2NotesMapping;
	public Map<Integer, List<Integer>> nk2NotesMapping_distorted;
	
	public Score score;
	
	private final static boolean randomnizeStacks = true;//false;
	
	private double maxData;
	private double minData;
	private double minVelocity=30;
	private double maxVelocity=127;
	private double minRythmvalue=jm.constants.Durations.SIXTEENTH_NOTE; 
	private double maxRythmvalue=jm.constants.Durations.WHOLE_NOTE*2;

	private int n_days =7;

	
	public Data2Midi(){
		
	}
	
	public Data2Midi(List<GTEvent> all_events){
	
		maxData=1250;//new Double(Math.log(2+1350));//1350;//
		minData=0;//new Double(Math.log(2+0));//0;
		
		//listOfCategories = genListOfCategories(all_events, MetricProvider.INSTANCE.log);
		//TODO:Erweiterung Konstruktor mit Berechnung listOfCategories
		//aus all_events:
		/**	  							
		1	  Int(-0.00731,1.04]  80	C9,C10	
		2     Int(1.04,2.09]      26	C7,C8 
		3     Int(2.09,3.13]      16	C5,C6
		4     Int(3.13,4.18]       8	C3,C4
		5     Int(4.18,5.22]       6	C2,C3
		6     Int(5.22,6.27]       2	C2
		7     Int(6.27,7.32]       2	C1
		**/
		listOfCategories = new ArrayList<Category>();
		//listOfCategories.add(new Category(-0.00731, 1.04, 80));
		listOfCategories.add(new Category(1.04,2.09, 26));
		listOfCategories.add(new Category(2.09,3.13, 16));
		listOfCategories.add(new Category(3.13,4.18, 8));
		listOfCategories.add(new Category(4.18,5.22, 6));
		listOfCategories.add(new Category(5.22,6.27, 2));
		listOfCategories.add(new Category(6.27,7.32, 2));
		
//		/**	  							
//		Int(-0.00731,0.665]   69	Q
//		Int(0.665,1.33]		  35
//		Int(1.33,1.99]        21
//		Int(1.99,2.66]        11
//		Int(2.66,3.32]        16
//		Int(3.32,3.99]         7
//		Int(3.99,4.65]         6
//		Int(4.65,5.32]         2
//		Int(5.32,5.98]         2
//		Int(5.98,6.65]         1
//		Int(6.65,7.32]         2
//		**/
//		listOfCategories = new ArrayList<Category>();
//		listOfCategories.add(new Category(1,-0.00731,0.665,69));
//		listOfCategories.add(new Category(2,0.665,1.33,35));
//		listOfCategories.add(new Category(3,1.33,1.99,21));
//		listOfCategories.add(new Category(4,1.99,2.66,11));
//		listOfCategories.add(new Category(5,2.66,3.32,16));
//		listOfCategories.add(new Category(6,3.32,3.99,7));
//		listOfCategories.add(new Category(7,3.99,4.65,6));
//		listOfCategories.add(new Category(8,4.65,5.32,2));
//		listOfCategories.add(new Category(9,5.32,5.98,2));
//		listOfCategories.add(new Category(10,5.98,6.65,1));
//		listOfCategories.add(new Category(11,6.65,7.32,2));
		
		Integer[] maxNr2Cat = extractMaxNr2Cat(listOfCategories);
		
		//TODO: Identify max and min values in data
		//maxData = 1500;
		//minData = 0;
		
		//minVelocity maxVelocity minRythmvalue maxRhythmvalue
		
		StacksOfNotesFactory stackOfNotesFactory = new StacksOfNotesFactory(maxNr2Cat);
		
		score = map2Score(all_events, stackOfNotesFactory);
		
//		Instrument i1 = new SimpleSineInst(44100);
//		int sampleRate = 44100;
//		Instrument[] insts = {
//				new SimpleSineInst(sampleRate),//instrument0
//				new AddSynthInst(sampleRate)//instrument1
//				};		
		
//		Write.au(score, "KlangKStudie01b.au", insts);//score, file name, instruments		
		
		
		
//		Play.midi(score);
		
		if(turnOnMidiFileWriter){
		
			Write.midi(score, "Prototype01.mid");
			System.out.println("MIDI file is located in current project workspace directory.");
//			try {
//				Desktop.getDesktop().open(new File("Prototype01.mid"));
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}
		
	}
	
	private List<Category> genListOfCategories(List<GTEvent> listOfEvents) {
		
		List<Category> listOfCategories = new ArrayList<Category>();
		
		//List<NK2MaxAtOneDay> list = CSVTextParser.parseData();
//		for(NK2MaxAtOneDay nk2m : list){
//			if(nk2m< metric){	
//			}
//		}
		
		for(GTEvent event : listOfEvents){
			
			//if(event.nk)
			
			
		}
			
		
		
		return listOfCategories ;
	}

	/**
	 * 
	 * Extracts from the list of Categories the maximumNr of each Category 
	 * 
	 * @param listOfCategories
	 * @return
	 */
	private Integer[] extractMaxNr2Cat(List<Category> listOfCategories) {
		int size = listOfCategories.size();	
		Integer[] maxNr2Cat = new Integer[size];
		int index= 0;
		for(Category c : listOfCategories){
			maxNr2Cat[index]=c.maxNK;
					index++;
		}
		return maxNr2Cat;
	}

	/**
	 * Maps a list of events into a list of Midi events 
	 * Note n = new Note(pitch,rhythmValue);
	 * 
	 * @param a
	 */
	private Score map2Score(List<GTEvent> all_events, StacksOfNotesFactory stackOfNotesFactory){
		
		int nrOfIntruments = stackOfNotesFactory.nrOfIntruments;
		int index=0;
		int lastIndex =all_events.size()-1;
		List<GTEvent> eventsWithin_N_Days = new ArrayList<GTEvent>();
		
		/**
		 * Initialization of an array of CPhrases. 
		 * Each CPhrase is a Sequence of Chords for one Instrument.
		 */
		CPhrase[] cphrArray = new CPhrase[nrOfIntruments];
		for (int i = 0; i < cphrArray.length; i++) {
			cphrArray[i] = new CPhrase();
		}
		
		int dayIndex = 0;
		
		for(GTEvent event : all_events){
			
			eventsWithin_N_Days.add(event);
			
			if(index!=lastIndex  && all_events.get(index).iday != all_events.get(index+1).iday){
				dayIndex++;
			}	
			
			//if(index == lastIndex || dayIndex%(n_days +1)==0){
			//if(dayIndex%(n_days +1)==0){	
			if(dayIndex==n_days){		
//			}
//			if(index == lastIndex || 
//					index!=lastIndex 
//			&& all_events.get(index).iday != all_events.get(index+1).iday){	
//			 {	
				Map<Integer, Deque<NoteExtended>> stacksOfNotes = stackOfNotesFactory.produceStacksOfNotes(randomnizeStacks, true);
				addNotes2PhrasesWithin_N_Days(eventsWithin_N_Days,cphrArray, stacksOfNotes);	
				eventsWithin_N_Days.clear();
				dayIndex=0;
			}
			index++;
		}
		
		Score score = new Score();
		Part[] partArray = new Part[nrOfIntruments];
		
		//int[] instruments = {48,48,48,48,48};//83
		int instrNr =0;
		for (int i = 0; i < nrOfIntruments; i++) {
			
//			if(i<instruments.length){
//				instrNr= instruments[i];
//			}
//			else{
			instrNr= 41;//50+i;
//			}
			//partArray[i] = new Part("Instrument "+instrNr,instrNr,phrArray[i]);
			partArray[i] =new Part("Instrument "+instrNr,instrNr);
			partArray[i].addCPhrase(cphrArray[i]);
			partArray[i].setChannel(i);
	
	}
		
//		Part p1 = new Part("Instrument0",1,phr);		
//		Part p2 = new Part("Instrument2",81);
//		p2.addCPhrase(cphr);
//		Part[] partArray = new Part[]{p1,p2};
//		Part[] partArray = new Part[]{p1};

	
		
		score.addPartList(partArray);
		
		
		return score;
		
	}
	
	
	/**
	 * Maps a list of events of one day into a list of Midi events 
	 * Note n = new Note(pitch,rhythmValue);
	 * 
	 * @param a
	 */
	private void addNotes2PhrasesWithin_N_Days(List<GTEvent> eventsAtOneDay, CPhrase[] cphrArray, Map<Integer, Deque<NoteExtended>> stacksOfNotes){
		
		/**
		 * Initialize Key-Value list, with key = instrument, value = List of Notes
		 */
		Map<Integer, List<NoteExtended>> instr2Notes = new HashMap<Integer, List<NoteExtended>>();
		int instrNr = 1;
		for(CPhrase cphr : cphrArray){
			instr2Notes.put(instrNr, new ArrayList<NoteExtended>());
			instrNr++;
		}
		
		double maxDurationValue = StacksOfNotesFactory.maxDurationValue; 
		double minDurationStep = StacksOfNotesFactory.durationFactor;
		//double minVelocityStep = (double) StacksOfNotesFactory.velocityFactor;
		/**
		 * Generate to each Event a Note and add it into the List of Notes for the corresponding Instrument
		 */
		for(GTEvent event : eventsAtOneDay){
			
			Double logNk = new Double(Math.log(2+event.nk));
			int key = interval2CategoryMapper(logNk);
			//Higher Categories should not(!) sound with more Notes even though they have less note values:
//			for (int i = 0; i < key; i++) {
			
			//no valid category key has been found for logNK:
			if(key==-1){
				break;
			}
			
			if(stacksOfNotes.get(key).isEmpty()){
				System.err.println("stacksOfNotes.get(key).isEmpty() for category " +key);
				break;
			}
			NoteExtended note= stacksOfNotes.get(key).pop();
			//NoteExtended note= stacksOfNotes.get(key).removeLast();
			
			//TODO: Use logNk:
			double input =event.nk;// logNk;////
			double rythmvalue = ((maxRythmvalue-minRythmvalue)/(maxData-minData))*(input-minData)+minRythmvalue; 
			int dynamic =(int) (((maxVelocity-minVelocity)/(maxData-minData))*(input-minData)+minVelocity);
			
			note.setDuration(rythmvalue);//genMidiValueByResize(logNk,minDurationStep, minDurationStep, maxDurationValue));
			note.setDynamic(dynamic);//(int) genMidiValueByResize(logNk, 10, 1, 127.0));
			instr2Notes.get(note.index4Instrument).add(note);
//			}
		}
		
		NoteExtended emptyNoteWithLengthOfOneDay = new NoteExtended(0, StacksOfNotesFactory.durationFactor, 0);
		emptyNoteWithLengthOfOneDay.setRhythmValue(StacksOfNotesFactory.globalRythmValue);
		NoteExtended[] noteChordPause = new NoteExtended[1];
		noteChordPause[0] = emptyNoteWithLengthOfOneDay;
		
		
		int _instrNr = 1;
		
		
		/**
		 * Generate Chords for each Instrument 
		 * by mapping the corresponding list of notes to an array of Notes at (*).
		 * 
		 *  In case that there are no Notes to a given instrument: a NoteChordPause
		 *  will be added at (**)
		 */
		for(CPhrase cphr : cphrArray){
			int size = instr2Notes.get(_instrNr).size();
			if(size!=0){
				
				NoteExtended[] noteChord = new NoteExtended[size+1];
				//default note with leading duration:
				noteChord[0]=emptyNoteWithLengthOfOneDay;
				int noteAt = 1;
				for(NoteExtended note : instr2Notes.get(_instrNr)){
					noteChord[noteAt]  = note;//(*)
					noteAt++;
				}
				
				cphr.addChord(noteChord);
				
			}else{
				cphr.addChord(noteChordPause);//(**)
			}
			_instrNr++;
		}
	
	
			
	}
	
	/**
	 * Maps data value to an midi attribute value (e.g. velocity or duration).
	 * The number of possible values of an midi attribute 
	 * is computed by resizing the nunmber of categories
	 * in dependence of a given minValueStep and max value.
	 *  
	 * 
	 * @param logNk
	 * @param minValue velocity or duration
	 * @param minValueStep
	 * @param maxValue
	 * @return velocity or duration
	 */
	@Deprecated
	private double genMidiValueByResize(Double logNk, double minValue, double minValueStep, double maxValue){
				
		int numberOfCategories = StacksOfNotesFactory.numberOfCategories;	
		
		//The Formula for the resizeFactor is derived from the definition of the 
		//maxValue=numberOfCategories*minValueStep*resizeFactor:
		
		double resizeFactor = maxValue/(numberOfCategories*minValueStep);
		if(resizeFactor<1.1){
			System.err.println("Warning resizeFactor is very low:"+resizeFactor);	
		}
		//The number of possible values will coincide with
		//the number of category 
		//by taking the resizeFactor == 1 after rounding, 
		//i.e. casting to an integer:
		int logNk_resized_rounded = (int) (resizeFactor*logNk);
		double result = minValueStep*logNk_resized_rounded;
		if(result<minValue){
			result = minValue;
		}
		if(result>maxValue){
			result = maxValue;
		}
		
		return result;
		
		
	}
	
	
	 
	   
	 /**
	  * Returns the category-key integer related to the intervals values of each category.
	  * If it is not contained in any category it will return the key -1. 
	  *  
	  * @param logNk
	  * @return
	  */
     private int interval2CategoryMapper(Double logNk) {
    	    
    		int key=-1;
    		
    	  	for(Category c : listOfCategories){
    	  		if(logNk<=c.maxValInInteval && logNk>c.minValInInteval){
    	  			key = c.key;
    	  			return key;
    	  		}
    	  	}
		return key;
	 }
	
    @Deprecated
	private int pickUpRandomPitch(int logNkInteger, Map<Integer, List<Integer>> _nk2NotesMapping){
		int randomChosenIndex = new Integer((int) (Math.random()*_nk2NotesMapping.get(logNkInteger).size()));
		return _nk2NotesMapping.get(logNkInteger).get(randomChosenIndex);
	}
	
	
	
	private static void testLog(){
		
		double init = 3;
		
		for (int i = 0; i < 10; i++) {
			Double logNkDouble = new Double(Math.log(init+i));
			int logNkInteger = logNkDouble.intValue();
			System.out.println(i+"->"+logNkInteger);
		}	
		for (int i =10; i < 1400; i++) {
			i=i*2;
			Double logNkDouble = new Double(Math.log(init+i));
			int logNkInteger = logNkDouble.intValue();
			System.out.println(i+"->"+logNkInteger);
		}	
			
			
	}
	
	private static void testResize(){
		
		double maxDurationValue = StacksOfNotesFactory.maxDurationValue; 
		double minDurationStep = StacksOfNotesFactory.durationFactor;
		StacksOfNotesFactory.numberOfCategories=7;
		Data2Midi data2midi =new Data2Midi();
		for (int i = 0; i <9; i++) {
			double logNk = 0.1+i;
			double value = data2midi.genMidiValueByResize(logNk,0.125,0.125, maxDurationValue );
			//double value = data2midi.genMidiValueByResize(nk, 10, 1, 127.0);
			System.out.println(logNk+"-->"+ value);
		
		}
		
	}
	
	public static void main(String a[]) {
		
//		Data2Midi.testResize();
		
		//System.out.println(jm.JMC.PENTATONIC_SCALE);
		

		//System.out.println(data2midi.genPentatonicScale(0));
		//System.out.println(data2midi.genPentatonicScale(0).size());
		//System.out.println(data2midi.genPentatonicScale(1));
		
//		Integer[] maxNr2Cat = {80,26,16,8,6,2,2};
//		data2midi.genNk2NotesMappings(maxNr2Cat);

//		data2midi.testLog();
//		System.out.println(data2midi.genPentatonicScale(0).size());
//		System.out.println(data2midi.genPentatonicScale(1).size());
		
	   //new Data2Midi(GTDBMock.INSTANCE.genData());
//	   new Data2Midi(GTDBTestData.INSTANCE.genData());
		
		new Data2Midi(GTDB.INSTANCE.genData());
		
		
			
	}
	
	
	
//	public static void main(String a[]) {
//		
//		R2JDataFrameProvider r2j = new R2JDataFrameProvider();
//		RList list = r2j.data;
//		REXP rexp0 = list.at(0);
//		REXPList list0= rexp0._attr();
//		System.out.println(list0.toString());
//		try {
//			RList list00 = rexp0.asList();
//			System.out.println(list00.toString());
//		} catch (REXPMismatchException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
////		REXP rexp1 = list.at(1);
////		REXP rexp2 = list.at(2);
//			
//	}
	

}