package midi.abstraction;

import java.util.HashMap;
import java.util.Map;

import jm.music.data.Note;


public class NoteExtended extends Note {
	
	/**
	 * index for instrument assignment:
	 */
	public int index4Instrument;
	
	/**
	 * Parameter to Value Mapping:
	 */
	public Map<String,Double> extraParameters;
	
	
	
	public NoteExtended(int pitch, double duration, int velocity) {
		super(pitch, duration, velocity);
	}
	
	public NoteExtended(int pitch, double duration, int velocity, double rythmValue, int index4Instrument) {
		super(pitch, duration, velocity);
		this.setRhythmValue(rythmValue);
		this.index4Instrument =  index4Instrument;
		
	}

	public void addValue(String parameterName, Double value){
		
		if(extraParameters==null){
			extraParameters = new HashMap<String,Double>();
		}
		extraParameters.put(parameterName, value);
	}
	

}
