package midi.jmusic;


import jm.music.data.Note;
import jm.music.data.Part;
import jm.music.data.Phrase;
import jm.music.data.Score;
import jm.util.Play;


public class KlangKompositionsStudie01 {

	
	public static void main(String[] args) {
		
		
		Part p1 = new Part("Instrument0",1, melody(1,1,-1));
		Part p2 = new Part("Instrument1",1, melody(1,1,0));

		
		Part bd = new Part("Drum Kit", 0, 9);
		Part sn = new Part("Drum Kit", 0, 9);
        Phrase phrBD = groove(36, 6);//new Phrase(0.0);
        Phrase phrSD = groove(38, 8);
//        Phrase phrHH = new Phrase(0.0); 
        bd.addPhrase(phrBD);
        sn.addPhrase(phrSD);
       
		
		Part[] partArray = new Part[]{p1,p2, bd, sn};
//		Part[] partArray = new Part[]{bd, sn};
		
		Score score = new Score();
		score.addPartList(partArray);
		
		//Instrument i1 = new SimpleSineInst(44100);
//		int sampleRate = 44100;
//		Instrument[] insts = {
//				new SimpleSineInst(sampleRate),//instrument0
//				new AddSynthInst(sampleRate)//instrument1
//				};		
		
//		Write.au(score, "KlangKStudie01b.au", insts);//score, file name, instruments		
		
		
		Play.midi(score);
		//Write.midi(score, "KlangKStudie01b.mid");

		
		
	}
	
	/**
	 * Input parameters a,b,c may be used to change the melody pattern.
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	private static Phrase melody(int a, int b, int c){
		Phrase phr = new Phrase();	
		int pitch;
		double rhythmValue;
			for(int k = 1; k<3; k++){
				for(int i = 1; i<16; i++){
					for(int j = 1; j<16; j++){
					pitch = 42+a*(i*j*k % 24)+b*(i*j*k % 32)+c*(i*j*k % 8);					
					rhythmValue = 0.25; //+ 0.25*(i*j*k % 2);
					Note n = new Note(pitch,rhythmValue);
					phr.addNote(n);
					}
				}
			}
		return phr;
	}
	
	/**
	 * Parameter a yield 
	 * bd for a=36
	 * sn for a=38
	 * closed hh for a=42
	 * open   hh for a=46  
	 * @param a
	 * @return
	 */
	private static Phrase groove(int a, int b){
		Phrase phr = new Phrase();	
		int pitch;
		double rhythmValue;
			for(int k = 1; k<3; k++){
				for(int i = 1; i<16; i++){
					for(int j = 1; j<16; j++){
					pitch = a;					
					rhythmValue = 0.25 + 0.25*(j% b); //+ 0.25*(i*j*k % b);
					Note n = new Note(pitch,rhythmValue);
					phr.addNote(n);
					}
				}
			}
		return phr;
	}
		
	

}
