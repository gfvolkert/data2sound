package data.model;

public class NK2MaxAtOneDay {
	
	public Double nk;
	public Integer maxAtOneDay;

	public NK2MaxAtOneDay(Double _nk, int _maxAtOneDay) {
		nk=_nk;
		maxAtOneDay=_maxAtOneDay;
	}

	

	@Override
	public String toString() {
		return "nk=" + nk + ", maxAtOneDay=" + maxAtOneDay;
	}
	
	
	
}