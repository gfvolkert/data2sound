package data.model;

import java.math.BigInteger;

public class GTEvent {

	public BigInteger eventid;
	public Integer iday;	
	public Integer nk;
	
	public Integer iyear;
	public Integer imonth;
	
	public GTEvent(BigInteger eventid, Integer iday, Integer nk) {
		super();
		this.eventid = eventid;
		this.iday = iday;
		this.nk = nk;
	}
	
	public GTEvent(BigInteger eventid, Integer iyear,Integer imonth,Integer iday, Integer nk) {
		super();
		this.eventid = eventid;
		this.iyear = iyear;
		this.imonth = imonth;
		this.iday = iday;
		this.nk = nk;
	}
	
	public String toString(){
		return eventid+"  "+iyear+" "+imonth+"  "+iday+"  "+nk;
		
	}
	
	
	
	
}
