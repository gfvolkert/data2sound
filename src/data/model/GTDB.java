package data.model;


import java.util.List;

import data.univocity.parsers.TSVTextParser;

public enum GTDB {
	
	INSTANCE;
	
	public final static int minYear = 2001; 
	public final static int maxYear = 2009; 
	
	
	/**
	 * Days
	 */
	//final static int SIZE = 300;//365;  
	
	
	public List<GTEvent> genData(){
		
		List<GTEvent> list = TSVTextParser.parseData(minYear, maxYear);
		
		return list;
		
	}
	
	
	
	
	public static void main(String a[]) {
		
		//GTDB.INSTANCE.genData();
		
		
		
	}
      
	

}

