package data.simulation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import data.model.GTEvent;

public enum GTDBTestData {

	INSTANCE;
	
	final static int MAXIMUMNK=7;
	
	/**
	 * Days
	 */
	final static int SIZE = 8;//365;  
	
	public List<GTEvent> genData(){
		
		List<GTEvent> list = new ArrayList<GTEvent>();
		int nk;
			
		//id, day, nk
		list.add(new GTEvent(BigInteger.valueOf(1),1,0));
		list.add(new GTEvent(BigInteger.valueOf(2),2,1));
		list.add(new GTEvent(BigInteger.valueOf(3),3,5));
		list.add(new GTEvent(BigInteger.valueOf(4),4,10));
		list.add(new GTEvent(BigInteger.valueOf(5),5,20));
		list.add(new GTEvent(BigInteger.valueOf(6),6,50));
		list.add(new GTEvent(BigInteger.valueOf(7),7,100));
		list.add(new GTEvent(BigInteger.valueOf(8),8,1000));
		
//		list.add(new GTEvent(BigInteger.valueOf(9),9,0));
//		list.add(new GTEvent(BigInteger.valueOf(9),9,1));
//		list.add(new GTEvent(BigInteger.valueOf(9),9,5));
//		list.add(new GTEvent(BigInteger.valueOf(9),9,10));
//		
//		list.add(new GTEvent(BigInteger.valueOf(10),10,1));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,1));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,0));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,1));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,1));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,0));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,5));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,1));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,3));
//		list.add(new GTEvent(BigInteger.valueOf(10),10,0));
//		
//		
//		list.add(new GTEvent(BigInteger.valueOf(11),11,5));
//		list.add(new GTEvent(BigInteger.valueOf(11),11,50));
//		list.add(new GTEvent(BigInteger.valueOf(11),11,0));
//		list.add(new GTEvent(BigInteger.valueOf(11),11,1));
//		list.add(new GTEvent(BigInteger.valueOf(11),11,15));
//		list.add(new GTEvent(BigInteger.valueOf(11),11,0));
//		list.add(new GTEvent(BigInteger.valueOf(11),11,23));
		
		
		
//		list.add(new GTEvent(BigInteger.valueOf(12),12,1));
//		list.add(new GTEvent(BigInteger.valueOf(12),12,10));
//		list.add(new GTEvent(BigInteger.valueOf(12),12,0));
//		list.add(new GTEvent(BigInteger.valueOf(12),12,1000));
//		list.add(new GTEvent(BigInteger.valueOf(12),12,100));
//		list.add(new GTEvent(BigInteger.valueOf(12),12,4));
		
		
	

				
			
			
		
		
		return list;
		
	}
}
