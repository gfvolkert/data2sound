package data.univocity.parsers.notUsed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import data.univocity.parsers.TSVTextParser;

public class GTDBParser {
	
	public List<GTEventParsed> listOfGTevnts = new ArrayList<GTEventParsed>();
	
	public GTDBParser(){
		
		BeanListProcessor<GTEventParsed> rowProcessor = new BeanListProcessor<GTEventParsed>(GTEventParsed.class);

	    TsvParserSettings parserSettings = new TsvParserSettings();
	    parserSettings.getFormat().setLineSeparator("\r");
	    parserSettings.setRowProcessor(rowProcessor);
	    parserSettings.setHeaderExtractionEnabled(true);
	    //parserSettings.setHeaderExtractionEnabled(false);
	    parserSettings.selectFields("eventid","iyear", "imonth", "iday", "nkill");

	    TsvParser parser = new TsvParser(parserSettings);
	    parser.parse(TSVTextParser.getReader("//Users/gfvolkert/jwspaces/e4t/topologic.text/data/gtdb.txt"));

	    // The BeanListProcessor provides a list of objects extracted from the input.
	    listOfGTevnts = rowProcessor.getBeans();
	    
	    
	    Collections.sort(listOfGTevnts, new Comparator<GTEventParsed>() {
	    	
	    	@Override
	    	public int compare(GTEventParsed s1, GTEventParsed s2) {
	            return s1.getEventid().compareTo(s2.getEventid());
	        }
			
			
	    });
	}
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		
		GTDBParser data = new GTDBParser();
		
		int k = 0;
	    int max = 20;
	    
	    System.out.println("size of data.listOfGTevnts:"+data.listOfGTevnts.size());
	   
	    for(GTEventParsed gtevent :data.listOfGTevnts){
//	    	if((gtevent.eventid.length()==12) && gtevent.nkill!=null && k<max){
	    	if(gtevent.nkill!=null && k<max){
	    		System.out.println(gtevent.toString());
	    		k++;
	    	}else if(k==max){
	    		break;
	    	}
	    }
		 
	  
	    
	   
	}
	
	
	

}
