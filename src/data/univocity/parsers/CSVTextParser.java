package data.univocity.parsers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import data.model.GTEvent;
import data.model.NK2MaxAtOneDay;

public class CSVTextParser {
	
	
	public static List<NK2MaxAtOneDay> parseData(){
	
		CsvParserSettings settings = new CsvParserSettings();
	    //the file used in the example uses '\n' as the line separator sequence.
	    //the line separator sequence is defined here to ensure systems such as MacOS and Windows
	    //are able to process this file correctly (MacOS uses '\r'; and Windows uses '\r\n').
	    //settings.getFormat().setLineSeparator("\r");
	    
	    String[] fields = {"Nkill","Maximum_at_a_day"};
	    settings.selectFields(fields);
	    
	    // creates a TSV parser
	    CsvParser parser = new CsvParser(settings);
	
	    // parses all rows in one go.
	    List<String[]> allRows = parser.parseAll(getReader("data/max_nkill_per_day.csv"));
		
	    List<NK2MaxAtOneDay> list = new ArrayList<NK2MaxAtOneDay>();
	    for(String[] arr :allRows){
	    		
	    		if(arr[0].equals("Nkill")){
	    		
	    		}else{
	    	
		    		Double nk=-1.0;
				try {
					if(arr[0]!=null){
						nk = new Double(arr[0]);
					}
				} catch (NumberFormatException e) {
					
					System.err.println("Exception due to arr[0]= "+arr[0]);
					//e.printStackTrace();
				}
				
				Integer nrOfEvents = -1;
				try {
					if(arr[1]!=null)
						nrOfEvents= new Integer(arr[1]);
				} catch (Exception e) {
					System.err.println("Exception due to arr[1]= "+arr[1]);
				}
				
				if(nk>=0.0 && nrOfEvents>-1){
					NK2MaxAtOneDay nk2m = new NK2MaxAtOneDay(nk, nrOfEvents);
		    			list.add(nk2m);
				}
	    		}
	    }
		
	    return list;
	}
	
	public static Reader getReader(String path2File) {
		
		Reader reader=null;
		try {
			reader = new FileReader(path2File);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return reader;
	}
	
public static void main(String[] args) {
		
		List<NK2MaxAtOneDay> list = CSVTextParser.parseData();
	    
		int sizeOfheaderIndex = 	10;
		int sizeOfcodaIndex = 10;
		
		int index=0;
		for(NK2MaxAtOneDay entry  : list){
			if(index<sizeOfheaderIndex){
				System.out.println(entry.toString());
			}
			if(index==sizeOfheaderIndex){
				System.out.println(".......");
			}
			if(index>list.size()-sizeOfcodaIndex){
				System.out.println(entry.toString());
			}
			index++;	
		}
		System.out.println("last index = " +index);
		System.out.println("listofEvents.size() = " +list.size());

	}

}
