package sound.csound;

//Skip to content
//Sign up Sign in This repository
//Explore
//Features
//Enterprise
//Blog
// Star 8  Fork 8 csound/csoundAPI_examples
// branch: master  csoundAPI_examples / java / src / csoundapiexamples / Example5.java
//Steven Yikunstmusik on Nov 22, 2013 translated example 5 to Java
//1 contributor
//162 lines (130 sloc)  5.345 kb RawBlameHistory   
//package csoundapiexamples;

import csnd6.Csound;
import csnd6.csnd6;
import data.r.R2JDataFrameProvider;

import java.util.ArrayList;
import java.util.List;

import org.rosuda.REngine.REXPMismatchException;

@SuppressWarnings("restriction")
public class Example5 {

    public static String example1() {
        return "i1 0 1 0.5 8.00";
    }

    public static String example2() {

        StringBuilder sco2 = new StringBuilder();
        for (int i = 0; i < 13; i++) {
            sco2.append(
                    String.format("i1 %g .25 0.5 8.%02d\n", i * .25, i));
        }

        return sco2.toString();
    }

    public static String join(List<?> values) {
        StringBuilder retVal = new StringBuilder();

        for (Object obj : values) {
            retVal.append(obj.toString()).append(" ");
        }

        return retVal.toString();
    }

    public static String example3() {
        @SuppressWarnings("rawtypes")
		List<List> vals = new ArrayList<List>();

        // initialize a list to hold lists of values 
        for (int i = 0; i < 13; i++) {
            @SuppressWarnings("rawtypes")
			List<Comparable> values = new ArrayList<Comparable>();
            
            //inst Id
            values.add(1);
            
            //start
            values.add(i * .25);
            
            //duration
            values.add(.25);
            
            //p4: factor for kenv
            values.add(0.5);
            
            //p5: pitch
            values.add(String.format("8.%02d", (int) (Math.random() * 15)));
            vals.add(values);
        }

        // convert list of values into a single string
        StringBuilder buffer = new StringBuilder();
        for (List list : vals) {
            buffer.append("i").append(join(list)).append("\n");
        }
        String score =  buffer.toString();
        System.out.println(score);
        return score;
    }
    
    public static void runCSoundExample(){
	
	csnd6.csoundInitialize(
                csnd6.CSOUNDINIT_NO_ATEXIT | csnd6.CSOUNDINIT_NO_SIGNAL_HANDLER);

        // Defining our Csound ORC code within a String
        String orc = "sr=44100\n"
                + "ksmps=32\n"
                + "nchnls=2\n"
                + "0dbfs=1\n"
                + "\n"
                + "instr 1 \n"
                + "ipch = cps2pch(p5, 12)\n"
                + "kenv linsegr 0, .05, 1, .05, .7, .4, 0\n"
                + "aout vco2 p4 * kenv, ipch \n"
                + "aout moogladder aout, 2000, 0.25\n"
                + "outs aout, aout\n"
                + "endin\n";

        /* SCORE EXAMPLES */

        // Example 1 - Static Score
        String sco = example1();

        // Example 2 - Generating Score string with a loop
        String sco2 = example2();

        // Example 3 - Generating Score using intermediate data structure (list of lists),
        // then converting to String.
        String sco3 = example3();

        /* END SCORE EXAMPLES */

        // Create an instance of the Csound object
        Csound c = new Csound();

        // Using SetOption() to configure Csound
        // Note: use only one commandline flag at a time 
        c.SetOption("-odac");

        // Compile the Csound Orchestra string
        c.CompileOrc(orc);

        // Compile the Csound SCO String
//        c.ReadScore(sco);
//        c.ReadScore(sco2);
        c.ReadScore(sco3);

        // When compiling from strings, this call is necessary before doing 
        // any performing
        c.Start();

        // The following is our main performance loop. We will perform one block 
        // of sound at a time and continue to do so while it returns 0, which 
        // signifies to keep processing.  We will explore this loop technique in 
        // further examples.

        while (c.PerformKsmps() == 0) {
            // pass for now
        }
        

        // stops Csound
        c.Stop();

        // clean up Csound; this is useful if you're going to reuse a Csound 
        // instance
        c.Cleanup();
    }

    public static void main(String[] args) {
    	
    	
    
//    	R2JDataFrameProvider r2j = new R2JDataFrameProvider();
//		try {
//			
//			String test = r2j.data.at(0).asString();
//			System.out.println(test);
//		} catch (REXPMismatchException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	
    	
		runCSoundExample();

    }
}
//Status API Training Shop Blog About
//� 2014 GitHub, Inc. Terms Privacy Security Contact
