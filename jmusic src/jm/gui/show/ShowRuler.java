/* --------------------
* A jMusic tool which displays a score as a
* Common Practice Notation in a window.
* @author Andrew Brown 
 * @version 1.0,Sun Feb 25 18:43
* ---------------------
*/
package jm.gui.show;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

//--------------
//third class!!
//--------------
public class ShowRuler extends Canvas implements MouseListener, MouseMotionListener{
	//attributes
	private int startX;
	private int height = 15;
	private int timeSig = 2;
	private ShowPanel sp;
        private Font font = new Font("Helvetica", Font.PLAIN, 10);
	
	public ShowRuler(ShowPanel sp) {
		super();
		this.sp = sp;
		this.setSize((int)(sp.score.getEndTime()*sp.beatWidth),height);
		this.setBackground(Color.lightGray);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setCursor(new Cursor(13));
	}
        
        /**
        * Report the height of this ruler panel.
        */
        @Override
		public int getHeight() {
            return height;
        }
	
	@Override
	public void paint(Graphics g) {
	    double beatWidth = sp.beatWidth;
            g.setFont(font);
            for(int i=0;i<(sp.score.getEndTime());i++){ 
                int xLoc = (int)Math.round(i*beatWidth);
                if (i%timeSig == 0) {
                        g.drawLine( xLoc,0, xLoc,height);
                        if (beatWidth > 15) g.drawString(""+i, xLoc+2, height-2);
                    } else {
                            g.drawLine( xLoc,height/2, xLoc,height);
                    }
            }
	}
	
	// get the position of inital mouse click
	@Override
	public void mousePressed(MouseEvent e) {
	    //System.out.println("Pressed");
	    this.setCursor(new Cursor(10));
		startX = e.getX();
	}
	
	//Mouse Listener stubs
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	@Override
	public void mouseReleased(MouseEvent e) {
	    this.setCursor(new Cursor(13));
            sp.update();
	}
	//mouseMotionListener stubs
	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {
	    //System.out.println("Dragged");
	    double beatWidth = sp.beatWidth;
		beatWidth += ((double)e.getX() - (double)startX)/5.0;
		if ( beatWidth< 1.0) beatWidth= 1.0;
		if ( beatWidth> 256.0) beatWidth= 256.0;
		//System.out.println("beatWidth = "+beatWidth);
		sp.beatWidth = beatWidth;
		startX = e.getX();
		//sp.update();
                this.repaint();
	}
}
