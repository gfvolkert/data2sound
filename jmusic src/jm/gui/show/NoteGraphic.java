package jm.gui.show;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class NoteGraphic extends Component implements MouseListener{
	NoteGraphic() {
		super();
		this.addMouseListener(this);
	}
	
	@Override
	public void mousePressed(MouseEvent me) {
		System.out.println("X is: "+me.getX());
	}
	
	@Override
	public void mouseClicked(MouseEvent me) {}
	@Override
	public void mouseEntered(MouseEvent me) {}
	@Override
	public void mouseExited(MouseEvent me) {}
	@Override
	public void mouseReleased(MouseEvent me) {}
}
